// model = sumber data

const md5 = require ('md5');
const db = require('./db/models')
const { Op } = require ("sequelize");

let userlist = [];
   

class UserModel{
    // cek semua user data
    getAllUser = async () => {
        const dataUsers = await db.User.findAll({include: [db.UserBio, db.userhistory ]});
        // select * FROM USERS
        return dataUsers;
    };
  

// check username teregister? yes or no / T or F
isUserRegistered = async (dataUser) => {
    const sudahMasuk = await db.User.findOne({
        where: {
            [Op.or]: [{ username: dataUser.username}, { email : dataUser.email}],
        },
    });
   
    if(sudahMasuk) {
        return true ;
    }else {
        return false;
    }
    };

isUserBioExist = async (UserBio) => {
    const bioMasuk = await db.UserBio.findOne({
        where: {
            [Op.or]: [
                { fullname: UserBio.fullname},
                { phoneNumber: UserBio.phoneNumber},
                { user_id: UserBio. user_id },
            ],
        },
    });
    if (bioMasuk) {
        return true;
    }else {
        return false;
    }
};
//    method record new data

recordNewData = (dataUser) => {
    //  INSERT INTO table ()values()
    db.User.create({
        username : dataUser.username,
        email : dataUser.email,
        // // password hashed md5
        paassword : md5(dataUser.paassword),

    });
};

confirmLogin = async (username, email , paassword) => {
    const sortedUser = await db.User.findOne({
        where: {username: username,email:email, paassword: md5 (paassword)},
    });

    return sortedUser;

}
findUserBio = async (userId) => {
    console.log(userId);
    return await db.User.findOne({
        include :[db.UserBio],
         where: {id: userId},
    });
    
};

updateUserBio = async (userId, fullname, phoneNumber, address) => {
    return await db.UserBio.update({ fullname: fullname, phoneNumber : phoneNumber, address: address }, { where: { user_id: userId }});
};

registerNewUserBio =(UserBio) =>{
    db.UserBio.create({
        fullname: UserBio.fullname,
        phoneNumber: UserBio.phoneNumber,
        address: UserBio.address,
        user_id: UserBio.user_id,
    });
};

updateHistory = (userGame) => {
    db.userhistory.create ({
     games: userGame.games,
     time: userGame.time,
     status: userGame.status,
     user_id: userGame.user_id,

    });
};

gameHistory = async (userId) => {
    return await db.User.findOne({
        include: [db.userhistory],
        where: { id: userId},

    });
};

}



module.exports = new UserModel();