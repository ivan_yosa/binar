const express = require ('express');
const userRouter = express.Router();
const userController = require('./controller');

userRouter.get('/user',userController.getAllUser);
userRouter.post("/register",userController.registerNewUser);
userRouter.post("/login",userController.loginUser);
userRouter.post("/registerBio", userController.registerNewUserBio);
userRouter.get ("/userBio/:userId", userController.findUserBio);
userRouter.put("/updateUserBio/:userId", userController.updateUserBio);
userRouter.put("/inserthistory", userController.updateHistory);
userRouter.get("/gamehistory/:userId",userController.gameHistory);

      
module.exports = userRouter;
