const userModel = require("./model.js");

class UserController {
  getAllUser = async (req, res) => {
    const allUser = await userModel.getAllUser();
    return res.json(allUser);
  };

  // ganti req,res | else if ganti userData
  registerNewUser = async (req, res) => {
    const dataUser = req.body;

    if (dataUser.username == "" && dataUser.paassword == "") {
      req.json({ message: " write your username" });
    } else if (dataUser.email == "") {
      res.json({ message: "write your email" });
    } else if (dataUser.paassword == "") {
      res.json({ message: "write your password" });
    }
    //  ganti user , email, pass, UserData + await

    const sudahMasuk = await userModel.isUserRegistered(dataUser);

    if (sudahMasuk) {
      return res.json({ message: "Username or Email allready exist" });
    }

    // record data user baru
    userModel.recordNewData(dataUser);
    return res.json({ message: "succes add new user" });
  };

  registerNewUserBio = async (req, res) => {
    const UserBio = req.body;
    try {
      if (
        UserBio.fullname == "" &&
        UserBio.phoneNumber == "" &&
        UserBio.address == "" &&
        UserBio.user_id == ""
      ){
        res.statusCode = 404;
        return res.json({ message: "opps..write your fullname"});
      }else if (UserBio.phoneNumber == "") {
        res.statusCode = 404;
        return res.json({ message: "write your phone number"});
      }else if (UserBio.address == "") {
        res.statusCode = 404;
        return res.jsson({ message: "write your address"});
      }else if (UserBio.user_id == "") {
        res.statusCode = 404;
        return res.json({
          message: "field your user id",
        });
      }
    } catch (error) {
      console.log(error);
    }

    // ganti user,email,pass userBio +await
    const bioMasuk = await userModel.isUserBioExist(UserBio);
    if (bioMasuk) {
      res.statusCode = 404;
      return res.json({ message: "data already exist"});
    }

    // params ganti UserBio
    userModel.registerNewUserBio(UserBio);
    return res.json({ message: "Sucess for new user !"});
  };

  loginUser = async (req, res) => {
    const { username, email, paassword } = req.body;
    const sortedUser = await userModel.confirmLogin(username, email, paassword);

    if (sortedUser) {
      return res.json(sortedUser);
    } else {
      res.statusCode = 404;
      return res.json({ message: "your acount is not valid" });
    }
  };
  findUserBio = async (req, res) => {
    const { userId } = req.params;
    console.log(userId);

    try {
      const user = await userModel.findUserBio(userId);

      if (user) {
        return res.json(user);
      } else {
        res.statusCode = 404;
        console.log(userId);
        return res.json({ message: " user id tidak ditemukan: " + userId });
      }
    } catch (error) {
      res.statusCode = 404;
      console.log(userId);
      return res.json({ message: "user id tidak ditemukan :" + userId });
    }
  };

  updateUserBio = async (req, res) => {
   const { userId } = req.params;
   const { fullname,phoneNumber,address } = req.body;
   const updateUserBio = await userModel.updateUserBio (
    userId, 
    fullname,
    phoneNumber,
    address 
   
    );

   return res.json({ message : `update user berhasil ${updateUserBio}`});
  };

  updateHistory = async (req, res) => {
    const userGame = req.body;

    if (
      userGame.games == "" &&
      userGame.time == "" &&
      userGame.status == "" &&
      userGame.user_id == ""
        
    ) {
      return res.json({ message: "write your fullname"});
    } else if (userGame.status == "") {
      return res.json ({ message: "field your status"});
    } else if (userGame.user_id == "") {
      return res.json({ message: "field your userid"});
    } else if (userGame.games == "") {
      return res.json({ message: "field your game names"});
    }

    userModel.updateHistory(userGame);
    return res.json({ message: "Success new user history"});
  };

  gameHistory = async (req, res) => {
    const { userId} = req.params;

    const gameHistory = await userModel.gameHistory(userId);
    return res.json(gameHistory);

  };


}
  
module.exports = new UserController();
